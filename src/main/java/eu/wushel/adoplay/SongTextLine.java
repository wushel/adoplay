package eu.wushel.adoplay;

import javafx.beans.Observable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.util.Callback;
import org.apache.poi.ss.usermodel.Cell;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SongTextLine {

    private final StringProperty textProperty;
    private final StringProperty timeProperty;
//    private final IntegerProperty excelRowNumProperty;


//    private final Cell textCell;
//    private final Cell timeCell;

    public SongTextLine(String text, String time/*, int index*/) {
        textProperty = new SimpleStringProperty();
        timeProperty = new SimpleStringProperty();
//        excelRowNumProperty = new SimpleIntegerProperty();
//        this.textCell = textCell;
//        this.timeCell = timeCell;
        setText(text);
//        String time = getCellString(time);
        setTime(time);
        if (time.equals("")) {
            changeTimeValue(0);
        }
//        setRowIndex(index);
    }

    public SongTextLine(Cell textCell, Cell timeCell) {
        this(getCellString(textCell), getCellString(timeCell));
    }

    public static Callback<SongTextLine, Observable[]> extractor() {
        return (stl) -> new Observable[]{stl.textProperty, stl.timeProperty/*, stl.excelRowNumProperty*/};
    }

    private void setText(String text) {
        textProperty.set(text);
    }

    private void setTime(String time) {
        timeProperty.set(time);
    }

//    private void setRowIndex(int rowIndex) {
//        excelRowNumProperty.set(rowIndex);
//    }

    @Override
    public String toString() {
        return getTime() + "\t\t" + getText();
    }

    public void changeTimeValue(int millis) {
        String value = "[" + timeToString(millis) + "]";
        timeProperty.setValue(value);
//        timeCell.setCellValue(value);
    }

    public void timePlusHundreds() {
        int millis = stringToTime(timeProperty.get());
        millis += 100;
        changeTimeValue(millis);
    }

    public void timeMinusHundreds() {
        int millis = stringToTime(timeProperty.get());
        if (millis > 0) {
            millis -= 100;
            changeTimeValue(millis);
        }
    }

    public String getText() {
        return textProperty.get();
    }

    public String getTime() {
        return timeProperty.get();
    }

//    public int getExcelRowIndex() {
//        return excelRowNumProperty.get();
//    }

    public static String timeToString(int milliseconds) {
        int seconds = milliseconds / 1000 % 60;
        int minutes = (milliseconds / (1000 * 60)) % 60;
        int hundreds = (milliseconds % 1000) / 10;

        return String.format("%02d:%02d.%02d", minutes, seconds, hundreds);
    }

    public static String timeToMilliString(int milliseconds) {
        int seconds = milliseconds / 1000 % 60;
        int minutes = (milliseconds / (1000 * 60)) % 60;
        int millis = milliseconds % 1000;

        return String.format("%02d:%02d.%03d", minutes, seconds, millis);
    }

    public static int stringToTime(String string) {
        if (string == null) {
            return 0;
        }
        Pattern regexPattern = Pattern.compile("\\[([0-9]{2}):([0-9]{2})\\.([0-9]{2})]");
        Matcher matcher = regexPattern.matcher(string);
        if (matcher.find()) {
            int minutes = Integer.parseInt(matcher.group(1));
            int seconds = Integer.parseInt(matcher.group(2));
            int hundreds = Integer.parseInt(matcher.group(3));
            return hundreds * 10 + seconds * 1000 + minutes * 60 * 1000;
        }
        return -1;
    }

    public static String getCellString(Cell cell) {
        String textValue = "<uninitialized>";
        if (cell != null) {
            switch (cell.getCellTypeEnum()) {
                case FORMULA:
                    textValue = "<no formulas allowed>";
                    break;
                case BLANK:
                case STRING:
                    textValue = cell.getStringCellValue();
                    break;
                case NUMERIC:
                    textValue = String.valueOf(cell.getNumericCellValue());
                    break;
                case BOOLEAN:
                    textValue = String.valueOf(cell.getBooleanCellValue());
                    break;
                case ERROR:
                    textValue = "<Error #" + cell.getErrorCellValue() + ">";
                    break;
                default:
                    Controller.showSpecialExceptionDialog("Fehler beim Lesen der Excel-Zelle", "Adoplay kann den Inhalt der Zelle nicht lesen:\n" + cell.getSheet().getSheetName() + ":" + cell.getAddress().formatAsString() + "\n\nDabei hab ich mir echt Mühe gegeben..", null, null);
            }
            return textValue;
        }
        return "<null>";
    }
}
