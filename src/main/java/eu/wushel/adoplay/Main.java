package eu.wushel.adoplay;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    private Controller ctrl;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Thread.setDefaultUncaughtExceptionHandler(Main::showError);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/gui.fxml"));
        Parent root = loader.load();
        ctrl = loader.getController();
        ctrl.setStage(primaryStage);
        primaryStage.setTitle("Adoplay");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/icon/icon.png")));
        primaryStage.setMinHeight(520);
        primaryStage.setMinWidth(320);
        Scene scene = new Scene(root, 800, 600);
//        scene.getStylesheets().add(getClass().getResource("/css/adoplay.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void stop() {
        ctrl.saveWorkbook();
    }

    private static void showError(Thread thread, Throwable throwable) {
        if (Platform.isFxApplicationThread()) {
            Controller.showGenericExceptionDialog(throwable, null);
        } else {
            System.err.println("An unexpected error occured in " + thread + ": " + throwable.getMessage());
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
