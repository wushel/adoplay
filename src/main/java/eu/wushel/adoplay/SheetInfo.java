package eu.wushel.adoplay;

import org.apache.poi.ss.usermodel.Sheet;

public class SheetInfo {
    public final Sheet sheet;

    public SheetInfo(Sheet sheet) {
        this.sheet = sheet;
    }

    @Override
    public String toString() {
        return sheet.getSheetName();
    }
}
