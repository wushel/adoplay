package eu.wushel.adoplay;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import javazoom.jl.converter.Converter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.controlsfx.dialog.ProgressDialog;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.stream.Stream;

public class Controller implements Initializable {

    public ListView<SongTextLine> textList;
    public ChoiceBox<String> sheetChooser;
    public Label timestamp;
    public Button play;
    public Button stamp;
    public Slider mediaSlider;
    public Button createSongLineAbove;
    public Button deleteSongLine;
    public Button plusHundreds;
    public Button minusHundreds;
    public Button createSongLineBelow;
    public ToggleButton toggleEdit;
    public BorderPane window;
    public MenuBar menuBar;
    public MenuItem menuExportLyrics;
    public Label xlsPath;
    public Label mp3Path;
    public MenuItem menuDeleteSheet;
    public MenuItem menuSaveXLS;

    private Workbook workbook;
    private Path workbookFile;
    private Path lastDirectoryPath;
    private MediaPlayer player;

    private boolean dragged = false;

    private Map<String, ObservableList<SongTextLine>> songTextLines;
    private ObservableList<String> sheetNames;

    private Path stateFile;

    private Stage stage;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sheetChooser.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != -1 && sheetChooser.getSelectionModel().getSelectedItem() != null) {
                loadSongLines(sheetChooser.getItems().get(newValue.intValue()));
            }
        });
        textList.setCellFactory((ListView<SongTextLine> param) -> new SongTextLineCell(this));
        textList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                setEditButtons(false);
                setStampButton(false);
            } else {
                setEditButtons(true);
                setStampButton(true);
            }
        });
        lastDirectoryPath = null;
        stateFile = Paths.get("adoplay.state");
        if (Files.exists(stateFile)) {
            try (Stream<String> stream = Files.lines(stateFile)) {
                stream.forEach((string) -> {
                    try {
                        Files.deleteIfExists(Paths.get(string));
                    } catch (IOException e) {
                        showSpecialExceptionDialog("Fehler beim Löschen", "Adoplay hat versucht, folgende temporäre Datei zu löschen:\n" + string + "\n\nIst sie evtl. in einem anderen Programm geöffnet?\nWenn ja, wer hat dir das erlaubt?!", stage, e);
                    }
                });
            } catch (IOException e) {
                showSpecialExceptionDialog("Fehler beim Öffnen", "Adoplay kann seine eigene Datei nicht öffnen:\n" + stateFile.toAbsolutePath().toString() + "\n\nHast du mit der Datei rumgespielt?", stage, e);
            } finally {
                try {
                    Files.delete(stateFile);
                } catch (IOException e) {
                    showSpecialExceptionDialog("Fehler beim Löschen", "Adoplay möchte seine eigene Datei löschen:\n" + stateFile.toAbsolutePath().toString() + "\n\nGeht aber nicht... keine Ahnung, warum. Wenn du so nett wärst, und mir das abnimmst, bin ich dir ewig dankbar.\nAnsonsten ist es vermutlich auch nicht so schlimm.", stage, e);
                }
            }
        }
        songTextLines = new HashMap<>();
    }

    public void loadXLSXClick() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Excelsheet", "*.xls", "*.xlsx"));
        fileChooser.setTitle("XLSX mit Texten laden");
        if (lastDirectoryPath != null) {
            fileChooser.setInitialDirectory(lastDirectoryPath.toFile());
        }
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            workbookFile = file.toPath().toAbsolutePath();
            xlsPath.setText(workbookFile.toString());
            lastDirectoryPath = workbookFile.getParent();
            try {
                try (Workbook workbook = WorkbookFactory.create(workbookFile.toFile())) {
                    sheetNames = FXCollections.observableArrayList();
                    for (Sheet sheet : workbook) {
                        sheetNames.add(sheet.getSheetName());
                        ObservableList<SongTextLine> list = FXCollections.observableArrayList(SongTextLine.extractor());
                        for (Row row : sheet) {
                            // put song text in B column
                            Cell textCell = row.getCell(1);
                            Cell timeCell = row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            list.add(new SongTextLine(textCell, timeCell));
                        }
                        songTextLines.put(sheet.getSheetName(), list);
                    }
                }
                sheetChooser.getSelectionModel().clearSelection();
                sheetChooser.setItems(sheetNames);
                sheetChooser.getSelectionModel().selectFirst();
                if (sheetChooser.getItems().size() > 0) {
                    loadSongLines(sheetChooser.getItems().get(0));
                    textList.getSelectionModel().selectFirst();
                    menuExportLyrics.setDisable(false);
                } else {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Nix");
                    alert.setHeaderText("Nüscht. Keine Daten drin.");
                    alert.setContentText("Na hör mal - wo nix drin ist, kann auch nix rauskommen.");
                    alert.showAndWait();
                }
                // this remains deactivated UFN - we seem not to need this feature
//                createSheet.setDisable(false);
                menuDeleteSheet.setDisable(false);
                menuSaveXLS.setDisable(false);
                sheetChooser.setDisable(false);
            } catch (IOException e) {
                showSpecialExceptionDialog("Fehler beim Laden der Arbeitsmappe", "Adoplay kann die Datei nicht öffnen:\n" + workbookFile.toAbsolutePath().toString() + "\n\nIrgendwas ist da wohl schiefgegangen...", stage, e);
            } catch (InvalidFormatException e) {
                showSpecialExceptionDialog("Fehler beim Lesen der Arbeitsmappe", "Adoplay versteht die Exceldatei nicht:\n" + workbookFile.toAbsolutePath().toString() + "\n\nIst es auch wirklich eine Exceldatei?\nBloß weil .xls(x) draufsteht, heißt das noch nicht, dass tatsächlich .xls(x) drin ist...", stage, e);
            }
        }
    }

//    private void loadSheets(boolean preserveIndex) {
//        sheetNames = FXCollections.observableArrayList();
//        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
//            sheetNames.add(new SheetInfo(workbook.getSheetAt(i)));
//        }
//        int index = sheetChooser.getSelectionModel().getSelectedIndex();
//        sheetChooser.getSelectionModel().clearSelection();
//        sheetChooser.setItems(sheetNames);
//        if (preserveIndex) {
//            sheetChooser.getSelectionModel().select(index);
//        } else {
//            sheetChooser.getSelectionModel().selectFirst();
//        }
//    }

    private void loadSongLines(String sheetName) {
//        ObservableList<SongTextLine> list = FXCollections.observableArrayList(SongTextLine.extractor());
//        songTextLines =
//        for (Row row : sheetName) {
//            // put song text in B column
//            Cell textCell = row.getCell(1);
//            Cell timeCell = row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
//            songTextLines.add(new SongTextLine(textCell, timeCell));
//        }
        textList.setItems(songTextLines.get(sheetName));
        textList.setDisable(false);
    }


    public void loadMP3Click() {
        if (player != null) {
            player.dispose();
        }
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("MP3", "*.mp3"));
        if (lastDirectoryPath != null) {
            fileChooser.setInitialDirectory(lastDirectoryPath.toFile());
        }
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            Path mp3 = file.toPath().toAbsolutePath();
            lastDirectoryPath = mp3.getParent();
            try {
                convertMP3(mp3);
            } catch (IOException e) {
                showSpecialExceptionDialog("Fehler beim Konvertieren", "Adoplay kann das MP3 nicht konvertieren. Mögliche Gründe:\n- Die temporäre Zieldatei kann nicht erstellt werden\n- Adoplays eigene Datei (adoplay.state) kann nicht erstellt werden", stage, e);
            }

        }
    }

    public void playClick() {
        if (player != null) {
            switch (play.textProperty().getValue()) {
                case "play":
                    player.play();
                    play.textProperty().setValue("pause");
                    break;
                case "pause":
                    player.pause();
                    play.textProperty().setValue("play");
                    break;
                default:
                    throw new AssertionError();
            }
        }
    }

    public void stampClick(MouseEvent actionEvent) {
        if (actionEvent.isPrimaryButtonDown()) {
            SongTextLine current = songTextLines.get(sheetChooser.getSelectionModel().getSelectedItem()).get(textList.getSelectionModel().getSelectedIndex());
            if (current != null) {
                current.changeTimeValue((int) player.getCurrentTime().toMillis());
                int index = textList.getSelectionModel().getSelectedIndex();
                if (songTextLines.get(sheetChooser.getValue()).size() - 1 > index) {
                    textList.getSelectionModel().clearAndSelect(index + 1);
                    textList.scrollTo(index + 1 - 2);
                } else {
                    textList.getSelectionModel().clearSelection();
                }
            }
        }
    }

    public static void showSpecialExceptionDialog(String title, String message, Stage owner, Throwable throwable) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fehler");
        alert.setHeaderText(title);
        String longMessage;
        if (throwable == null) {
            longMessage = message;
        } else {
            longMessage = message + "\n\nTechnische Fehlermeldung: " + throwable.toString();
        }
        alert.setContentText(longMessage);
        alert.initOwner(owner);
        alert.showAndWait();
    }

    public static void showGenericExceptionDialog(Throwable ex, Stage owner) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fehler");
        alert.setHeaderText("Hoppla, mit diesem Fehler hab nichtmal ich gerechnet.");
        alert.setContentText(ex.getMessage());
        alert.initOwner(owner);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("Ganz viele technische Ausdrücke:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    public void createSheetClick() {
        // deactivated UFN
        /*
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Name sheet");
        dialog.setHeaderText("Please name the sheet");
        dialog.setContentText("Name of the sheet:");
        Optional<String> result = dialog.showAndWait();

        result.ifPresent((String name) -> {
            String safeName = WorkbookUtil.createSafeSheetName(name);
            if (safeName.equals(name)) {
                newSheet(name);
            } else {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Confirm name");
                alert.setHeaderText("Invalid name");
                alert.setContentText("Are you ok with this sheetname?\n" + safeName);
                Optional<ButtonType> result1 = alert.showAndWait();
                if (result1.get() == ButtonType.OK) {
                    newSheet(safeName);
                }
            }
        });*/
    }

    /*private void newSheet(String name) {
        workbook.createSheet(name);
        saveWorkbook();
        loadSheets(true);
    }*/


    public void saveWorkbook() {
//        Sheet sheet = sheetChooser.getSelectionModel().getSelectedItem().sheet;
//        for (int i = 0; i < songTextLines.size(); i++) {
//            Row row = sheet.getRow(i);
//            if (row == null) {
//                row = sheet.createRow(i);
//            }
//            row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).setCellValue(songTextLines.get(i).getTime());
//            row.getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).setCellValue(songTextLines.get(i).getText());
//        }
//        for (int i = sheet.getLastRowNum(); i > songTextLines.size(); i--) {
//            sheet.removeRow(sheet.getRow(i));
//        }

        if (workbookFile != null) {
            try {
                // first delete the workbook file
                Files.deleteIfExists(workbookFile);
                // then create new workbook and write
                Workbook workbook = workbookFactory(sheetNames.toArray(new String[0]));
                for (Sheet s : workbook) {
                    List<SongTextLine> list = songTextLines.get(s.getSheetName());
                    for (int i = 0; i < list.size(); i++) {
                        Row row = s.createRow(i);
                        row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).setCellValue(list.get(i).getTime());
                        row.getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).setCellValue(list.get(i).getText());
                    }
                }
                try (FileOutputStream fos = new FileOutputStream(workbookFile.toFile())) {
                    workbook.write(fos);
                }
            } catch (IOException e) {
                showSpecialExceptionDialog("Fehler beim Speichern der Arbeitsmappe", "Adoplay kann die Datei nicht speichern:\n" + workbookFile.toAbsolutePath().toString() + "\n\nIst die Datei eventuell irgendwo anders geöffnet?", stage, e);
            }
        }
    }

    public void createSongLineAboveClick() {
        createSongLine(0);
    }

    public void createSongLineBelowClick() {
        createSongLine(1);
    }

    private void createSongLine(int delta) {
        int index = textList.getSelectionModel().getSelectedIndex();
        SongTextLine origin = songTextLines.get(sheetChooser.getValue()).get(index);
//        Sheet current = currentSheet();
//        if (current != null) {
//            current.shiftRows(index + delta, current.getLastRowNum(), 1);
//            current.createRow(index + delta);
//            Cell timeCell = current.getRow(index + delta).getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
//            Cell textCell = current.getRow(index + delta).getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
//            timeCell.setCellValue(origin.getTime());
        songTextLines.get(sheetChooser.getValue()).add(index + delta, new SongTextLine("", origin.getTime()));
//        saveWorkbook();
//        }

    }

    public void deleteSongLineClick() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Bestätige Löschung");
        alert.setHeaderText("Wirklich?");
        alert.setContentText("Diese Zeile wirklich löschen?\n\"" + textList.getSelectionModel().getSelectedItem().getText() + "\"");
        ButtonType buttonTypeYes = new ButtonType("Yep");
        ButtonType buttonTypeCancel = new ButtonType("Lieber doch nicht...", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeCancel);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeYes) {
            songTextLines.get(sheetChooser.getValue()).remove(textList.getSelectionModel().getSelectedIndex());
//            songTextLines.get(sheetChooser.getValue()).remove(stl);
//            Sheet current = currentSheet();
//            if (current != null) {
//                current.removeRow(current.getRow(stl.getExcelRowIndex()));
//            }
//            saveWorkbook();
        }
    }

//    private Sheet currentSheet() {
//        if (sheetChooser.getSelectionModel().getSelectedIndex() != -1) {
//            return sheetChooser.getSelectionModel().getSelectedItem().sheet;
//        } else {
//            return null;
//        }
//    }

    public void plusHundredsClick() {
        SongTextLine stl = textList.getSelectionModel().getSelectedItem();
        stl.timePlusHundreds();
//        saveWorkbook();
    }

    public void minusHundredsClick() {
        SongTextLine stl = textList.getSelectionModel().getSelectedItem();
        stl.timeMinusHundreds();
//        saveWorkbook();
    }

    public void deleteSheetClick() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Bestätige Löschung");
        alert.setHeaderText("Wirklich?");
        alert.setContentText("Dieses Blatt wirklich löschen?\n\"" + sheetChooser.getValue() + "\"");
        ButtonType buttonTypeYes = new ButtonType("Yep");
        ButtonType buttonTypeCancel = new ButtonType("Lieber doch nicht...", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeCancel);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeYes) {
            songTextLines.remove(sheetChooser.getValue());
            sheetChooser.getSelectionModel().clearSelection();
            sheetNames.remove(sheetChooser.getValue());
            if (sheetChooser.getItems().size() > 0) {
                sheetChooser.getSelectionModel().select(0);
                loadSongLines(sheetChooser.getValue());
            }
//            workbook.removeSheetAt(sheetChooser.getSelectionModel().getSelectedIndex());
//            saveWorkbook();
//            loadSheets(false);
        }
    }

    private static Workbook workbookFactory(String[] sheetNames) {
        Workbook workbook = new XSSFWorkbook();
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Open Sans");
        font.setFontHeightInPoints((short) 11);
        style.setFont(font);
        for (String str : sheetNames) {
            Sheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName(str));
            sheet.setDefaultColumnStyle(1, style);
            sheet.setZoom(100);
            // equation for column width:
            // (#visChars*maxDigitWidth+5)/maxDigitWidth*256
            // #brainfart
            // 9728 is taken from original XLS
            sheet.setColumnWidth(1, 9728);
        }
        return workbook;
    }

    public void createXLSXClick() {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("MP3 Ordner auswählen");
        if (lastDirectoryPath != null) {
            chooser.setInitialDirectory(lastDirectoryPath.toFile());
        }
        File file = chooser.showDialog(stage);
        if (file != null) {
            Path directory = file.toPath();
            lastDirectoryPath = directory;
            File[] files = directory.toFile().listFiles((dir, name) -> name.toLowerCase().endsWith(".mp3"));
            if (files != null) {
                String[] names = new String[files.length];
                for (int i = 0; i < files.length; i++) {
                    names[i] = files[i].getName().split("\\.")[0];
                }
                Workbook workbook = workbookFactory(names);
                File targetFile = new File(directory.toFile(), directory.getName(directory.getNameCount() - 1) + ".xlsx");
                try (FileOutputStream fileOutputStream = new FileOutputStream(targetFile)) {
                    workbook.write(fileOutputStream);
                    Desktop.getDesktop().open(targetFile);
                } catch (IOException e) {
                    showSpecialExceptionDialog("Fehler beim Erzeugen der Arbeitsmappe", "Es konnte keine Arbeitsmappe mit dem Namen\n" + targetFile.getAbsolutePath() + "\nerstellt werden.\n\nExistiert diese Arbeitsmappe evtl. schon?", stage, e);
                }
            }
        }
    }

    public void itemDoubleClicked(SongTextLine item) {
        if (player != null) {
            player.seek(Duration.millis(SongTextLine.stringToTime(item.getTime())));
        }
    }

    private void setEditButtons(boolean edit) {
        boolean val = textList.getSelectionModel().getSelectedItem() != null && toggleEdit.isSelected() && edit;
        deleteSongLine.setDisable(!val);
        createSongLineAbove.setDisable(!val);
        createSongLineBelow.setDisable(!val);
        plusHundreds.setDisable(!val);
        minusHundreds.setDisable(!val);
    }

    private void setStampButton(boolean active) {
        boolean val = player != null && toggleEdit.isSelected() && textList.getSelectionModel().getSelectedItem() != null && active;
        stamp.setDisable(!val);
    }

    public void toggleEditClick() {
        if (toggleEdit.isSelected()) {
            toggleEdit.textProperty().set("edit mode");
            setEditButtons(true);
            setStampButton(true);
            window.setStyle("-fx-background-color: derive(-fx-base,26.4%);");
            timestamp.setTextFill(Color.BLACK);
            mp3Path.setTextFill(Color.BLACK);
            xlsPath.setTextFill(Color.BLACK);
//            textList.getStyleClass().remove("standard-listview");
//            textList.getStyleClass().add("blue-listview");
            textList.setStyle(".standard-listview");
            textList.setStyle("-fx-control-inner-background: derive(-fx-base,80%);-fx-selection-bar-non-focused: lightgrey");
        } else {
            toggleEdit.textProperty().set("playback mode");
            player.pause();
            play.textProperty().set("play");
            player.seek(Duration.ZERO);
            timestamp.textProperty().setValue(SongTextLine.timeToMilliString(0));
            textList.getSelectionModel().clearSelection();
            textList.scrollTo(0);
            setEditButtons(false);
            setStampButton(false);
            window.setStyle("-fx-background-color: #00628a");
            timestamp.setTextFill(Color.web("#ececec"));
            mp3Path.setTextFill(Color.web("#ececec"));
            xlsPath.setTextFill(Color.web("#ececec"));
//            textList.getStyleClass().remove("blue-listview");
//            textList.getStyleClass().add("standard-listview");
            textList.setStyle(".blue-listview");
            textList.setStyle("-fx-control-inner-background: derive(#00628a,100%);-fx-selection-bar-non-focused: derive(#00628a,70%);");
        }
    }

    public void exportClick() {
        File targetFile = new File(workbookFile.getParent().toString() + "/" + workbookFile.getFileName().toString().split("\\.")[0] + "-lyrics-" + LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-uuuu")) + ".txt");
        try (FileWriter fw = new FileWriter(targetFile)) {
            fw.write("Lyrics für " + workbookFile.getFileName().toString().split("\\.")[0] + "\n\n\n");
            for (String sheetName : songTextLines.keySet()) {
                fw.write(sheetName + "\n\n");
                for (SongTextLine stl : songTextLines.get(sheetName)) {
                    fw.write(stl.getTime());
                    fw.write(stl.getText());
                    fw.write("~\n");
                }
                fw.write("\n");
            }
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Lyrics erfolgreich exportiert.");
            alert.initOwner(stage);
            alert.initModality(Modality.WINDOW_MODAL);
            alert.setHeaderText(null);
            alert.setContentText("Lyrics erfolgreich exportiert nach:\n" + targetFile.getAbsolutePath() + "\n\nIch bin so frei und öffne dir gleich das Verzeichnis.");
            alert.showAndWait();
            Desktop.getDesktop().open(targetFile.getParentFile());
        } catch (IOException e) {
            showSpecialExceptionDialog("Fehler beim Exportieren der Lyrics", "Adoplay konnte die Lyrics nicht exportieren. Kein Plan wieso.", stage, e);
        }
    }

//    public void closeWb(Workbook workbook) {
//        if (workbook != null) {
//            try {
//                workbook.close();
//            } catch (IOException e) {
//                showSpecialExceptionDialog("Kann Arbeitsmappe nicht schließen.", "Das Schließen der Arbeitsmappe ist fehlgeschlagen.\nKein großes Ding, aber sollte nicht regelmäßig passieren.", stage, e);
//            }
//        }
//    }

    public void closeClick() {
        Platform.exit();
    }

    private class ConvertTask extends Task<Void> {
        private final File wav;
        private final Path mp3;

        private ConvertTask(Path mp3) throws IOException {
            this.mp3 = mp3;
            wav = File.createTempFile(UUID.randomUUID().toString(), ".wav");
            wav.deleteOnExit();
            Files.write(stateFile, Collections.singletonList(wav.getAbsolutePath()), StandardOpenOption.WRITE, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
        }

        @Override
        protected Void call() throws Exception {
            updateProgress(-1, 0);
            Converter converter = new Converter();
            converter.convert(mp3.toString(), wav.toString());
            return null;
        }

        @Override
        protected void done() {

            if (wav != null) {
                Platform.runLater(() -> mp3Path.setText(mp3.toAbsolutePath().toString()));
                player = new MediaPlayer(new Media(wav.toURI().toString()));
                player.setOnReady(() -> {
                    mediaSlider.setMax(player.getMedia().getDuration().toMillis());
                    timestamp.textProperty().setValue(SongTextLine.timeToMilliString(0));
                    player.currentTimeProperty().addListener((observable, oldValue, newValue) -> {
                        double millis = newValue.toMillis();
                        timestamp.textProperty().setValue(SongTextLine.timeToMilliString((int) millis));
                        if (!dragged) {
                            mediaSlider.valueProperty().set(millis);
                            if (!toggleEdit.isSelected() && songTextLines != null && sheetChooser.getValue() != null) {
                                int index = textList.getSelectionModel().getSelectedIndex();
                                if (index + 1 < songTextLines.get(sheetChooser.getValue()).size()) {
                                    SongTextLine stl = songTextLines.get(sheetChooser.getValue()).get(index + 1);
                                    if (stl != null) {
                                        if (millis >= SongTextLine.stringToTime(stl.getTime())) {
                                            textList.getSelectionModel().select(index + 1);
                                            textList.scrollTo(index + 1 - 2);
                                        }
                                    }
                                }
                            }
                        }
                    });
                    mediaSlider.setOnMouseDragged(event -> {
                        if (event.isPrimaryButtonDown()) {
                            dragged = true;
                            player.seek(Duration.millis(mediaSlider.getValue()));
                        }
                    });
                    mediaSlider.setOnMouseReleased(event -> dragged = false);
                    mediaSlider.setOnMouseClicked(event -> {
                        if (player.getStatus() == MediaPlayer.Status.PAUSED) {
                            player.seek(Duration.millis(mediaSlider.getValue()));
                        }
                    });
                    play.setDisable(false);
                    play.setText("play");
                    setStampButton(true);
                    mediaSlider.setValue(0);
                    toggleEdit.setDisable(false);
                    mediaSlider.setDisable(false);
                    timestamp.setDisable(false);
                });
            }
        }
    }

    private void convertMP3(final Path mp3) throws IOException {
        ConvertTask ct = new ConvertTask(mp3);
        ProgressDialog pd = new ProgressDialog(ct);
        pd.setTitle("Import");
        pd.setHeaderText("Importiere " + mp3.getFileName().toString());
        pd.initModality(Modality.WINDOW_MODAL);
        pd.initOwner(stage);
        Thread th = new Thread(ct);
        th.setDaemon(true);
        th.start();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
