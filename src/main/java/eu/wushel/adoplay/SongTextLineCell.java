package eu.wushel.adoplay;

import javafx.scene.control.Control;
import javafx.scene.control.ListCell;

public class SongTextLineCell extends ListCell<SongTextLine> {

    private Controller ctrl;

    public SongTextLineCell(Controller ctrl) {
        this.ctrl = ctrl;
    }

    @Override
    protected void updateItem(SongTextLine item, boolean empty) {
        super.updateItem(item, empty);
        if (!empty && (item != null)) {
            setText(item.getTime() + "\t\t" + item.getText());
            setOnMouseClicked(event -> {
                if (event.getClickCount() == 2) {
                    ctrl.itemDoubleClicked(getItem());
                }
            });
        } else {
            setText(null);
            setGraphic(null);
            setOnMouseClicked(null);
        }
    }
}
